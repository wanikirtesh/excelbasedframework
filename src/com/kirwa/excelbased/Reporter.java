package com.kirwa.excelbased;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * Created by KIRTESH WANI on 3/15/15.
 */
public class Reporter {
    public static String rptFileName="";
    static boolean generateReport;
    private static File rptFile;
    public static void init() throws IOException {
        ResourceBundle config = PropertyResourceBundle.getBundle("com.kirwa.excelbased.testConfiguration");
        generateReport = config.getString("GenerateHTMLReports").equalsIgnoreCase("true");
        if (generateReport) {
            rptFileName = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss").format(new Date()) + ".html";
            rptFile = new File("Reports" + File.separator + rptFileName);
            rptFile.createNewFile();
            writeToReport("<html>\n" +
                    "    <head>\n" +
                    "        <style>\n" +
                    "            table{\n" +
                    "                width:90%\n" +
                    "            }\n" +
                    "            td,th{\n" +
                    "                border:1px solid black;\n" +
                    "            }\n" +
                    "        </style>\n" +
                    "    </head>\n" +
                    "    <body>\n" +
                    "        <table>\n" +
                    "            <tr>\n" +
                    "                <th>TCID</th>\n" +
                    "                <th>Test Case Title</th>\n" +
                    "                <th>Result</th>\n" +
                    "                <th>Fail Cause</th>\n" +
                    "            </tr>\n");

        }
    }

    public static void closeReport() throws IOException {
        writeToReport("</table>\n" +
                "    </body>\n" +
                "</html>");
    }

    private static void writeToReport(String content) throws IOException {
        if (generateReport) {
            FileWriter fw = new FileWriter(rptFile.getAbsoluteFile(),true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content);
            bw.close();
        }
    }

    public static void printSingleHeader(String txtHeader) throws IOException {
        writeToReport("<tr><th colspan='4'>"+txtHeader+"</th></tr>");
    }

    public static void startTC(String TCId, String TCTitle) throws IOException {
        writeToReport("<tr><td>"+TCId+"</td><td>"+TCTitle+"</td>");
    }

    public static void endTC(String KWDetails, boolean tcResult, Boolean FailCause) throws IOException {
        writeToReport("<td>"+tcResult+"</td><td>"+FailCause+"</td></tr>");
        writeToReport("<tr><td colspan='4'>"+ KWDetails + "</table></td></tr>");
    }

    public static void putKeyWords(String kwDetails) {

    }
}
