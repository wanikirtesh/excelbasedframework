package com.kirwa.excelbased;

import com.kirwa.utils.XlsxReader;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * ****************************
 * Created by Kirtesh Wani on 04-03-2015.
 * for ExcelBasedFramework
 * *****************************
 */
public class DriverScript {

    private static XlsxReader suiteExcel;
    private static String kwHeaders="<table><tr>" +
            "<th>Keyword</th><th>Inputs</th><th>Result</th><th>Fail Cause</th></tr>";
    private static String KWDetails=kwHeaders;
    public static Logger LOGGER = Logger.getLogger(DriverScript.class);
    public static void main(String[] args) throws IOException {
        Reporter.init();
        ResourceBundle config = PropertyResourceBundle.getBundle("com.kirwa.excelbased.testConfiguration");
        MyDriver.init(MyDriver.browser.valueOf(config.getString("Browser").toUpperCase()));
        suiteExcel = new XlsxReader("TestSuite.xlsx");
        LOGGER.info("----------------------********-----------------------");
        LOGGER.info("Starting automation execution of the TestSuite");
        LOGGER.info("----------------------********-----------------------");
        for(int i=1;i< suiteExcel.getRowCount("Main");i++){
            if(suiteExcel.getCellStringData("Main",i,1).equalsIgnoreCase("true")){
                ExecuteSuite(suiteExcel.getCellStringData("Main",i,0));

            }
            else{
                LOGGER.warn("----------------------------------------------------------------------");
                LOGGER.warn("Skipping execution of the TestSuite " + suiteExcel.getCellStringData("Main", i, 0));
                LOGGER.warn("----------------------------------------------------------------------");
            }
        }
        MyDriver.getDriver().quit();
        Reporter.closeReport();
    }

    private static void ExecuteSuite(String suiteName) throws IOException {
        LOGGER.info("----------------------------------------------------------------------");
        LOGGER.info("Starting execution of the TestSuite " + suiteName);
        LOGGER.info("----------------------------------------------------------------------");
        Reporter.printSingleHeader(suiteName);
        String runningTC = "";
        boolean TCResult=true;
        for(int i=1;i<suiteExcel.getRowCount(suiteName);i++){
            String currentTC = suiteExcel.getCellStringData(suiteName,i,0);
            if (!runningTC.equals(currentTC)) {
                if(i!=1){
                    LOGGER.info("Ending Execution of TestCase ----------------------------" + TCResult);
                    LOGGER.info("---------------------------------------------------------------");
                    Reporter.endTC(KWDetails,TCResult,true);
                }
                KWDetails=kwHeaders;
                LOGGER.info("-------------------------------------------------------------------");
                LOGGER.info("Starting execution of the TestCase " + currentTC);
                Reporter.startTC(currentTC,suiteExcel.getCellStringData(suiteName,i,1));
                runningTC=currentTC;
                TCResult=true;
            }
            TCResult &= ExecuteKeyWord(suiteName,i);
        }
        if(suiteExcel.getRowCount(suiteName)>1){
            LOGGER.info("Ending Execution of TestCase -------------------------------" + TCResult);
            LOGGER.info("---------------------------------------------------------------");
            Reporter.endTC(KWDetails,TCResult,true);
        }

    }

    private static boolean ExecuteKeyWord(String SuiteName, int RowNumber) {
        String currentKeyWord = suiteExcel.getCellStringData(SuiteName, RowNumber, 3);
        LOGGER.info("----------------------------------------------------------------------");
        LOGGER.info("Starting execution of the KeyWord " + RowNumber);
        LOGGER.info("----------------------------------------------------------------------");
        String[] parameters = suiteExcel.getCellStringData(SuiteName,RowNumber,4).split(",");
        Map<String,String> paramMap = new HashMap<String,String>();
        for(int i=0;i<parameters.length;i++){
            paramMap.put(parameters[i],suiteExcel.getCellStringData(SuiteName,RowNumber,8+i));
        }

        boolean KWResult=false;
        for(int i=1;i<suiteExcel.getRowCount("Keywords");i++){

            if(currentKeyWord.equals(suiteExcel.getCellStringData("Keywords", i, 0))){
                KWResult = ExecuteMethod(currentKeyWord,suiteExcel.getCellStringData("Keywords", i, 1),paramMap);
                break;
            }
        }
        KWDetails += "<tr><td>"+currentKeyWord+"</td><td>"+paramMap.values()+"</td><td>"+KWResult+"</td><td>"+true+"</td></tr>";
        return KWResult;
    }

    private static boolean ExecuteMethod(String currentKeyWord, String strClass, Map<String, String> paramMap){
        try {

            Class<?> cl = Class.forName(strClass);
            Method m = cl.getMethod(currentKeyWord,paramMap.getClass());
            LOGGER.info("Running Method " + m);
            Object x = m.invoke(m, paramMap);
            LOGGER.info("Completed Method " + m + " With Output "+ x );
            return (Boolean) x;
        }
        catch(Exception e){
            LOGGER.error(e);
            return false;
        }
    }
}
