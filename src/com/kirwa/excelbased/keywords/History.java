package com.kirwa.excelbased.keywords;

import com.kirwa.excelbased.MyDriver;
import com.kirwa.excelbased.objectrepository.RepoHistory;
import com.kirwa.utils.DbCon;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import static com.kirwa.utils.MyStringUtil.isStrEquals;

/**
 * Created by KIRTESH WANI on 3/10/15.
 */
public class History {
    static Logger LOGGER = Logger.getLogger(History.class);
    public static boolean ValidateHistoryTable(HashMap<String, String> paramMap){
        try {
            DbCon dbCon = new DbCon();
            String sortHeader="";

            if(paramMap.get("sortColumn").equalsIgnoreCase("ammount")) {
                RepoHistory.amountHeader().click();
                RepoHistory.amountHeader().click();
                sortHeader="totalcost";
            }
            else
            {
                sortHeader="intenddate";
            }
            ResultSet rs = dbCon.getRecords("SELECT IF(STATUS=1,CONCAT('PO/',TypeofPO,'/',IF(TypeofPO='consumable','CO',IF(TypeofPO='work','WO','C')),'-',YEAR(IntendDate),'/',ponumber),CONCAT('Request/',YEAR(IntendDate),'/',POid)) AS ponumber,intenddate,TypeofPO,CONCAT(fname,' ',lname) AS Intendor,vendorname,totalcost,IF(STATUS=1,'Approved',IF(STATUS=0,'Submited',IF(STATUS=2,'Rejected','Checked'))) AS STATUS FROM masterpo INNER JOIN users ON userid=Indentor WHERE CONCAT(fname,' ',lname)='"+General.getLoggedInUserName()+"' Order by "+sortHeader+" desc,poid desc");
            WebElement historyTable = RepoHistory.getHistoryDataTable();
            List<WebElement> tableRows = historyTable.findElements(By.tagName("tr"));
            rs.last();
            if(tableRows.size()-1==rs.getRow()){
                rs.beforeFirst();
                boolean finalResult=true;
                int rowCounter=1;
                while(rs.next()){
                    finalResult&=validateData(tableRows.get(rowCounter++),rs);
                }
                rs.close();
                dbCon.dropConnection();
                return finalResult;
            }
            else{
                LOGGER.error("Row Count Does not match UI: "+ tableRows.size()+ " DB: " +rs.getRow());
                rs.close();
                dbCon.dropConnection();
                return false;
            }





        } catch (Exception e) {
            LOGGER.error(e);
            return false;
        }

    }

    private static boolean validateData(WebElement row, ResultSet rs) {
        try {
            if(row.findElement(By.xpath("./td[3]")).getText().equalsIgnoreCase(rs.getString("intenddate").trim())
                    &&row.findElement(By.xpath("./td[4]")).getText().equalsIgnoreCase(rs.getString("TypeOfPO").trim())
                    &&row.findElement(By.xpath("./td[5]")).getText().equalsIgnoreCase(rs.getString("intendor").trim())
                    &&row.findElement(By.xpath("./td[6]")).getText().equalsIgnoreCase(rs.getString("vendorname").trim())
                    &&row.findElement(By.xpath("./td[7]")).getText().equalsIgnoreCase(rs.getString("totalcost").trim())
                    &&row.findElement(By.xpath("./td[8]")).getText().equalsIgnoreCase(rs.getString("status").trim())

                    ) {
               /* LOGGER.info("On Screen:" + row.findElement(By.xpath("./td[3]")).getText() +" Databse: " + rs.getString("intenddate"));
                LOGGER.info("On Screen:" + row.findElement(By.xpath("./td[4]")).getText() +" Databse: " + rs.getString("TypeOfPO"));
                LOGGER.info("On Screen:" + row.findElement(By.xpath("./td[5]")).getText() +" Databse: " + rs.getString("intendor"));
                LOGGER.info("On Screen:" + row.findElement(By.xpath("./td[6]")).getText() +" Databse: " + rs.getString("vendorname"));
                LOGGER.info("On Screen:" + row.findElement(By.xpath("./td[7]")).getText() +" Databse: " + rs.getString("totalcost"));
                LOGGER.info("On Screen:" + row.findElement(By.xpath("./td[8]")).getText() +" Databse: " + rs.getString("status"));
*/
                return true;
            }
            else{
                LOGGER.error("On Screen:" + row.findElement(By.xpath("./td[3]")).getText() + " Databse: " + rs.getString("intenddate"));
                LOGGER.error("On Screen:" + row.findElement(By.xpath("./td[4]")).getText() + " Databse: " + rs.getString("TypeOfPO"));
                LOGGER.error("On Screen:" + row.findElement(By.xpath("./td[5]")).getText() + " Databse: " + rs.getString("intendor"));
                LOGGER.error("On Screen:" + row.findElement(By.xpath("./td[6]")).getText() + " Databse: " + rs.getString("vendorname"));
                LOGGER.error("On Screen:" + row.findElement(By.xpath("./td[7]")).getText() + " Databse: " + rs.getString("totalcost"));
                LOGGER.error("On Screen:" + row.findElement(By.xpath("./td[8]")).getText() + " Databse: " + rs.getString("status"));

                return false;
            }
        }

        catch (SQLException e) {
            LOGGER.error(e);
            return false;
        }

    }

 public static boolean validateViewActionForDisplyaedRecords(HashMap<String,String> paramMap)
 {
     try {


         WebElement historyTable = RepoHistory.getHistoryDataTable();
         List<WebElement> tableRows = historyTable.findElements(By.tagName("tr"));


             boolean finalResult=true;
             for(int i=1;i<tableRows.size();i++){
                 finalResult&=validateDataPopUp(tableRows.get(i));
             }

             return finalResult;



     } catch (Exception e) {
         e.printStackTrace();
     }
     return false;
 }

    private static boolean validateDataPopUp(WebElement tableRow) {
        DbCon dbCon = null;
        boolean result = false;
        try {
            dbCon = new DbCon();

            String poString = tableRow.findElement(By.xpath("./td[2]")).getText();
            String poStringList[] = poString.split("/");
            ResultSet rs;

            String strCondition = poStringList[0].equalsIgnoreCase("request") ? " POid='" + poStringList[2] + "' AND YEAR(IntendDate)=" + poStringList[1] : "PONumber=" + poStringList[3] + " AND typeofPO='" + poStringList[1].split(" ")[0] + "' AND YEAR(IntendDate)='" + poStringList[2].split("-")[1] + "'";
            // rs = dbCon.getRecords("SELECT IF(STATUS=1,CONCAT('PO/',TypeofPO,'/',IF(TypeofPO='consumable','CO',IF(TypeofPO='work','WO','C')),'-',YEAR(IntendDate),'/',ponumber),CONCAT('Request/',YEAR(IntendDate),'/',POid)) AS ponumber,intenddate,TypeofPO,CONCAT(fname,' ',lname) AS Intendor,vendorname,totalcost,IF(STATUS=1,'Approved',IF(STATUS=0,'Submited',IF(STATUS=2,'Rejected','Checked'))) AS STATUS FROM masterpo INNER JOIN users ON userid=Indentor WHERE "+strCondition);

            rs = dbCon.getRecords("SELECT * FROM masterpo INNER JOIN users ON userid=Indentor WHERE " + strCondition);
            rs.next();
            tableRow.findElement(By.xpath("./td[9]//a[@title='View']")).click();
            if(isStrEquals(rs.getString("VendorName"),MyDriver.getDriver().findElement(By.xpath("//table[1][@class='bordertable']/tbody/tr[2]/td[2]")).getText())
                && isStrEquals(rs.getString("ContactDetail"),MyDriver.getDriver().findElement(By.xpath("//table[1][@class='bordertable']/tbody/tr[3]/td[2]")).getText())

                    ) {
                LOGGER.info("On Screen Vendor :"+tableRow.findElement(By.xpath("//table[1][@class='bordertable']/tbody/tr[2]/td[2]")).getText()+" Database Vendor:"+rs.getString("VendorName"));
                LOGGER.info("On Screen Vendor Details :"+tableRow.findElement(By.xpath("//table[1][@class='bordertable']/tbody/tr[3]/td[2]")).getText()+" Database Vendor Details:"+rs.getString("ContactDetail"));

                result = true;
            } else {

                LOGGER.error("On Screen Vendor :" + tableRow.findElement(By.xpath("//table[1][@class='bordertable']/tbody/tr[2]/td[2]")).getText() + " Database Vendor:" + rs.getString("VendorName"));
                LOGGER.error("On Screen Vendor Details :"+tableRow.findElement(By.xpath("//table[1][@class='bordertable']/tbody/tr[3]/td[2]")).getText()+" Database Vendor Details:"+rs.getString("ContactDetail"));

                result = false;
            }

            RepoHistory.closePopUpButton().click();

            rs.close();
            dbCon.dropConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
