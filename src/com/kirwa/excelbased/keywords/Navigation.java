package com.kirwa.excelbased.keywords;

import com.kirwa.excelbased.objectrepository.RepoGeneral;

import java.util.HashMap;

import com.kirwa.excelbased.MyDriver;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
/**
 * Created by amol on 3/17/2015.
 */
public class Navigation {
    static Logger LOGGER = Logger.getLogger(General.class);
    public static boolean NavigateToHistory(HashMap<String, String> paramMap) {
        // printParam(paramMap);
        try {

            RepoGeneral.linkHistory().click();
            return RepoGeneral.classPoTable().isDisplayed();
        } catch (Exception e) {
            LOGGER.error(e);
            return false;
        }

    }


}
