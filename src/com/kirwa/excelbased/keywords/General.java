package com.kirwa.excelbased.keywords;

import com.kirwa.excelbased.MyDriver;
import com.kirwa.excelbased.objectrepository.RepoGeneral;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by KIRTESH WANI on 3/9/15.
 */
public class General {
    static Logger LOGGER = Logger.getLogger(General.class);
    public static WebDriver driver=MyDriver.getDriver();
    public static boolean LaunchApplication(HashMap<String, String> paramMap){

        printParam(paramMap);
        try
        {
            driver.get(paramMap.get("URL"));
            return RepoGeneral.classTitle().getText().equalsIgnoreCase("purchase order system");
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean LoginApplication(HashMap<String, String> paramMap){
        printParam(paramMap);
        try
        {
            RepoGeneral.txtUserName().sendKeys(paramMap.get("UserName"));
            RepoGeneral.txtPassword().sendKeys(paramMap.get("Password"));
            RepoGeneral.btnSubmit().click();
            return RepoGeneral.linkLogout().isDisplayed();

        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean LogoutApplication(HashMap<String, String> paramMap) {
        // printParam(paramMap);
        try {
            RepoGeneral.linkLogout().click();
            return RepoGeneral.classTitle().getText().equalsIgnoreCase("purchase order system");
        } catch (Exception e) {
            LOGGER.error(e);
            return false;
        }

    }

    public static void printParam(Map<String, String> paramMap) {
        Set<String> keys= paramMap.keySet();
        for (String key : keys) {
            LOGGER.info("Key:"+key+", Value:"+paramMap.get(key));
        }
    }

    public static String getLoggedInUserName(){
        return RepoGeneral.UserNameLabel().getText().replace("Welcome:","").trim();
    }

}
