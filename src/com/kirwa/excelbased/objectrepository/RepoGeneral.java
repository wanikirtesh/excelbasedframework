package com.kirwa.excelbased.objectrepository;

import com.kirwa.excelbased.MyDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by KIRTESH WANI on 3/15/15.
 */
public class RepoGeneral {

    public static WebElement classPoTable(){
        return MyDriver.getDriver().findElement(By.xpath("//table[@class='POtable']"));
    }
    public static WebElement classTitle(){
        return MyDriver.getDriver().findElement(By.xpath("//div[@class='title']"));
    }
    public static WebElement txtUserName(){
        return MyDriver.getDriver().findElement(By.xpath("//input[@id='username']"));
    }
    public static WebElement txtPassword(){
        return MyDriver.getDriver().findElement(By.xpath("//input[@id='password']"));
    }

    public static WebElement btnSubmit(){
        return MyDriver.getDriver().findElement(By.xpath("//button[@id='btnSubmit']"));
    }

    public static WebElement linkLogout() {

        return MyDriver.getDriver().findElement(By.xpath("//a[@href='Logout.do']"));
    }
    public static WebElement linkHistory() {

        return MyDriver.getDriver().findElement(By.xpath("//a[@href='History.do']"));
    }

    public static WebElement UserNameLabel(){
        return MyDriver.getDriver().findElement(By.xpath("//div[@class='dvheader']/div"));
    }
}
