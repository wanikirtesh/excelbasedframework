package com.kirwa.excelbased.objectrepository;

import com.kirwa.excelbased.MyDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by KIRTESH WANI on 3/22/15.
 */
public class RepoHistory {
    public static WebElement getHistoryDataTable(){
        return MyDriver.getDriver().findElement(By.id("stable"));
    }

    public static WebElement amountHeader() {
        return MyDriver.getDriver().findElement(By.xpath("//table[@id='stable']//th[text()='Ammount']"));
    }
    public static WebElement dateHeader() {
        return MyDriver.getDriver().findElement(By.xpath("//table[@id='stable']//th[text()='Date']"));
    }

    public static WebElement viewActionIcon(int i) {
        return MyDriver.getDriver().findElement(By.xpath("//table[@id='stable' ]/tbody/tr["+i+"]//a"));
    }

  public static WebElement closePopUpButton(){
      return  MyDriver.getDriver().findElement(By.xpath("//div[@class='ui-dialog-buttonset']/button"));
  }
}
