package com.kirwa.excelbased;

import com.kirwa.utils.MyEventListener;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.events.WebDriverEventListener;

import java.util.concurrent.TimeUnit;

/**
 * Created by KIRTESH WANI on 3/9/15.
 */
public class MyDriver {
    public enum browser{FIREFOX,IE,CHROME};

    public static EventFiringWebDriver driver;

    public static void init(browser Browser){
        WebDriver iDriver=null;
        if(iDriver==null){
            switch (Browser){
                case CHROME:
                    System.setProperty("webdriver.chrome.driver","");
                    iDriver = new ChromeDriver();
                    break;
                case FIREFOX:
                    iDriver =new FirefoxDriver();
                    break;
                case IE:
                    iDriver = new InternetExplorerDriver();
                    break;
            }
            iDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            WebDriverEventListener eventListener = new MyEventListener();
            driver = new EventFiringWebDriver(iDriver).register(eventListener);
        }
    }




    public static WebDriver getDriver(){
        return driver;
    }

}
