package com.kirwa.utils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.*;

/**********************************
 * Created by Kirtesh Wani on 04-03-2015.
 * for ExcelBasedFramework
 ***********************************/
public class XlsReader {
    private HSSFWorkbook workbook;
    private final String excelfilepath;
    public XlsReader(String excelFilePath) throws IOException {
        workbook = new HSSFWorkbook(new FileInputStream(new File(excelFilePath)));
        excelfilepath = excelFilePath;
    }
    public void close(){
        workbook = null;
    }
    public String getCellStringData(String SheetName,int RowNum,int ColNum){
        try{
            HSSFSheet sheet = workbook.getSheet(SheetName);
            Cell cell = sheet.getRow(RowNum).getCell(ColNum);
            switch(sheet.getRow(RowNum).getCell(ColNum).getCellType()){
                case STRING:
                    return cell.getRichStringCellValue().getString();
                case NUMERIC:
                    if (DateUtil.isCellDateFormatted(cell)) {
                        return cell.getDateCellValue().toString();
                    } else {
                        return Double.toString(cell.getNumericCellValue());
                    }
                case BOOLEAN:
                    return Boolean.toString(cell.getBooleanCellValue());
                case FORMULA:
                    return cell.getCellFormula();
                case BLANK:
                    return "";
                default:
                    return cell.getStringCellValue();
            }
        }
        catch(Exception e){
            return "";
        }
    }
    public int getRowCount(String SheetName){
        HSSFSheet sheet;
        try{
            sheet = workbook.getSheet(SheetName);
            return sheet.getPhysicalNumberOfRows();
        }finally{
            sheet=null;
        }
    }
    public int getColumnCount(String SheetName,int RowNum){
        HSSFSheet sheet;
        try{
            sheet = workbook.getSheet(SheetName);
            return sheet.getRow(RowNum).getPhysicalNumberOfCells();
        }finally{
            sheet=null;
        }
    }
    public boolean createSheet(String SheetName){
        try{
            FileOutputStream output = new FileOutputStream(excelfilepath);
            workbook.createSheet(SheetName);
            workbook.write(output);
            output.close();
            ExcelReaderInIt();
            return true;
        }
        catch(Exception e){
            return false;
        }
    }
    public boolean deleteSheet(String SheetName){
        try{
            FileOutputStream output = new FileOutputStream(excelfilepath);
            workbook.removeSheetAt(workbook.getSheetIndex(SheetName));
            workbook.write(output);
            output.close();
            ExcelReaderInIt();
            return true;
        }
        catch(Exception e){
            return false;
        }
    }
    private void ExcelReaderInIt() throws FileNotFoundException, IOException {
        workbook = new HSSFWorkbook(new FileInputStream(new File(excelfilepath)));
    }
}