package com.kirwa.utils;

/**
 * Created by KIRTESH WANI on 3/29/15.
 */
public class MyStringUtil {
    public static boolean isStrEquals(String str1, String str2){
        str1 = str1.trim().toLowerCase();
        str2 = str2.trim().toLowerCase();
        while(str1.indexOf("  ")>=0){
            str1 = str1.replace("  "," ");
        }
        while(str2.indexOf("  ")>=0){
            str2 = str2.replace("  "," ");
        }
        return str1.equalsIgnoreCase(str2);
    }
}
